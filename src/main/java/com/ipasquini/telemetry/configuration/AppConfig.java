package com.ipasquini.telemetry.configuration;

import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.facade.FacadeImpl;
import com.ipasquini.telemetry.metrics.Metrics;
import com.ipasquini.telemetry.metrics.MetricsInflux;
import com.ipasquini.telemetry.persistence.Persistence;
import com.ipasquini.telemetry.persistence.SQLite;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class AppConfig {

	private final JdbcTemplate jdbcTemplate;

	public AppConfig(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Bean
	public Persistence getSQL() {
		return new SQLite(jdbcTemplate);
	}

	@Bean
	public Facade getFacade() {
		return new FacadeImpl();
	}

	@Bean
	public Metrics getMetrics() {
		return new MetricsInflux(getFacade());
	}
}
