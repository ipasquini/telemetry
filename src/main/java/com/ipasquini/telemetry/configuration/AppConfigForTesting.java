package com.ipasquini.telemetry.configuration;

import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.facade.FacadeDummy;
import com.ipasquini.telemetry.metrics.Metrics;
import com.ipasquini.telemetry.metrics.MetricsDummy;
import com.ipasquini.telemetry.persistence.Persistence;
import com.ipasquini.telemetry.persistence.PersistenceDummy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class AppConfigForTesting {

	@Bean
	@Primary
	public Persistence getSQLDummy() {
		return new PersistenceDummy();
	}

	@Bean
	@Primary
	public Facade getFacadeDummy() {
		return new FacadeDummy();
	}

	@Bean
	@Primary
	public Metrics getMetricsDummy() {
		return new MetricsDummy();
	}
}
