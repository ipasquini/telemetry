package com.ipasquini.telemetry.controllers;

import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.services.Register;
import com.ipasquini.telemetry.services.Telemetry;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.net.UnknownHostException;

/**
 * Allows to add, remove and list {@link DataSource}
 */
@RestController
public class Controller {

	private final Facade facade;
	private final Register register;
	private final Telemetry telemetry;

	public Controller(Telemetry telemetry, Register register, Facade facade) {
		this.facade = facade;
		this.register = register;
		this.telemetry = telemetry;
	}

	/**
	 * @return all {@link DataSource}
	 */
	@GetMapping(value = "/datasource", produces = "application/json")
	public String getDataSources() {
		register.info("GET /datasource");
		return facade.getJson("datasources", telemetry.getDataSources());
	}

	/**
	 * Add a new {@link DataSource}
	 *
	 * @param host     name
	 * @param port     number
	 * @param encoding of the data
	 * @return the {@link DataSource} added
	 * @throws ResponseStatusException if the host is not valid
	 */
	@PostMapping(value = "/datasource", produces = "application/json")
	public String addDataSource(@RequestParam String host, @RequestParam short port, @RequestParam Encoding encoding)
			throws UnknownEncodingException {
		register.info(String.format("POST /datasource Host: %s, Port: %s, Encoding: %s", host, port, encoding));
		try {
			DataSource dataSource = telemetry.addDataSource(host, port, encoding);
			return facade.getJson("added", dataSource);
		} catch (UnknownHostException e) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Unknown host", e);
		}
	}

	/**
	 * Remove a {@link DataSource}
	 *
	 * @param host     name
	 * @param port     number
	 * @param encoding of the data
	 * @return the {@link DataSource} removed
	 */
	@DeleteMapping(value = "/datasource", produces = "application/json")
	public String removeDataSource(@RequestParam String host, @RequestParam short port, @RequestParam Encoding encoding) {
		register.info(String.format("DELETE /datasource Host: %s, Port: %s, Encoding: %s", host, port, encoding));
		return facade.getJson("removed", telemetry.removeDataSource(host, port, encoding));
	}
}
