package com.ipasquini.telemetry.collectors;

import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.decoders.Decoder;
import com.ipasquini.telemetry.exceptions.MalformedInputException;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.services.DecoderFactory;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.services.Register;
import org.msgpack.core.annotations.VisibleForTesting;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

/**
 * A collector is responsible for collect data from {@link DataSource}, decode it with {@link Decoder} and store it
 * with {@link Register}
 */
public class Collector {

	private final Facade facade;
	private final Decoder decoder;
	private final Register register;
	private final DataSource dataSource;

	private boolean running;
	private CompletableFuture<Boolean> future;

	public Collector(Register register, DataSource dataSource, DecoderFactory decoderFactory, Facade facade)
			throws UnknownEncodingException {
		this.running = true;
		this.facade = facade;
		this.register = register;
		this.dataSource = dataSource;
		this.decoder = decoderFactory.build(dataSource.getEncoding());
		start();
	}

	@VisibleForTesting
	boolean getRunning() {
		return running;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public CompletableFuture<Boolean> getFuture() {
		return future;
	}

	/**
	 * Start collecting data asynchronously from {@link DataSource}
	 */
	public void start() {
		future = facade.getFuture(facade.getSupplier(() -> collectAsync(dataSource)));
	}

	/**
	 * Stop collecting data
	 */
	public void stop() {
		running = false;
	}

	/**
	 * Collect data from {@link DataSource}, decode it with {@link Decoder} and store it with {@link Register}
	 *
	 * @param dataSource from which to collect data
	 * @return true if it was manually stopped, false if it was because an error
	 */
	@VisibleForTesting
	boolean collectAsync(DataSource dataSource) {
		int read;
		byte[] buffer = new byte[1024];

		try (Socket socket = facade.getSocket(dataSource.getHost(), dataSource.getPort())) {
			InputStream inputStream = socket.getInputStream();
			while (running && (read = inputStream.read(buffer)) != -1) {
				processData(buffer, read);
			}
			if (running) {
				register.error("lostconnection", null);
				return false;
			}
		} catch (IOException e) {
			register.error("ioexception", e);
			return false;
		} catch (Exception e) {
			register.error("unknownerror", e);
			return false;
		}

		return true;
	}

	/**
	 * Decode it with {@link Decoder} and store it with {@link Register}
	 *
	 * @param buffer into which the data is read
	 * @param read   is the total number of bytes read into the buffer
	 */
	@VisibleForTesting
	void processData(byte[] buffer, int read) {
		try {
			register.dataPoint(decoder.decode(buffer, read));
		} catch (MalformedInputException e) {
			register.error("malformedInput", e);
		}
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Collector collector = (Collector) o;
		return Objects.equals(dataSource, collector.dataSource);
	}

	@Override
	public int hashCode() {
		return Objects.hash(dataSource);
	}
}
