package com.ipasquini.telemetry.metrics;

import com.ipasquini.telemetry.data.DataPoint;

/**
 * Interface to push metrics
 */
public interface Metrics {

	void initialize();

	Void dataPoint(DataPoint dataPoint);

	Void error(String kind);
}
