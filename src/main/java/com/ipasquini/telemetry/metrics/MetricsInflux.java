package com.ipasquini.telemetry.metrics;

import com.ipasquini.telemetry.TelemetryApplication;
import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.facade.Facade;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Query;
import org.msgpack.core.annotations.VisibleForTesting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import java.util.concurrent.TimeUnit;

/**
 * Influx implementation of the metrics service
 */
public class MetricsInflux implements Metrics {

	static final Logger LOGGER = LoggerFactory.getLogger(TelemetryApplication.class);

	private static final String ID = "id";
	private static final String KIND = "kind";
	private static final String VALUE = "value";
	private static final String ERROR = "error";
	private static final String COUNT = "count";
	private static final String DATAPOINT = "datapoint";
	private static final String CREATE = "CREATE DATABASE %s";

	private final Facade facade;

	@Value("${influx.localurl}")
	private String localURL;

	@Value("${influx.dockerurl}")
	private String dockerURL;

	@Value("${influx.username}")
	private String username;

	@Value("${influx.password}")
	private String password;

	@Value("${influx.database}")
	private String database;

	private InfluxDB influxDB;

	public MetricsInflux(Facade facade) {
		this.facade = facade;
	}

	@VisibleForTesting
	void setLocalURL(String localURL) {
		this.localURL = localURL;
	}

	@VisibleForTesting
	void setUsername(String username) {
		this.username = username;
	}

	@VisibleForTesting
	void setPassword(String password) {
		this.password = password;
	}

	@VisibleForTesting
	void setDatabase(String database) {
		this.database = database;
	}

	/**
	 * Initialize {@link InfluxDB} database
	 */
	public void initialize() {
		LOGGER.info("Initializing InfluxDB...");
		boolean initialized = false;
		while (!initialized) {
			try {
				initialized = initialize(localURL);
			} catch (Exception ignored1) {
				try {
					initialized = initialize(dockerURL);
				} catch (Exception ignored2) {
					try {
						Thread.sleep(5000);
					} catch (InterruptedException ignored3) {

					}
				}
			}
		}
		LOGGER.info("Connected to InfluxDB");
	}

	private boolean initialize(String host) {
		influxDB = facade.getInfluxDB(host, username, password);
		influxDB.query(new Query(String.format(CREATE, database)));
		influxDB.setDatabase(database);
		return true;
	}

	/**
	 * Push a {@link DataPoint} into {@link InfluxDB}
	 *
	 * @param dataPoint to be pushed
	 */
	public Void dataPoint(DataPoint dataPoint) {
		influxDB.write(facade.buildPoint(
				DATAPOINT,
				dataPoint.getTimestamp(),
				TimeUnit.SECONDS,
				ID,
				Short.toString(dataPoint.getId()),
				VALUE,
				dataPoint.getValue()));
		return null;
	}

	/**
	 * Push an error into {@link InfluxDB}
	 *
	 * @param kind of the error
	 */
	public Void error(String kind) {
		influxDB.write(facade.buildPoint(
				ERROR,
				System.currentTimeMillis(),
				TimeUnit.MILLISECONDS,
				KIND,
				kind,
				COUNT,
				1));
		return null;
	}
}
