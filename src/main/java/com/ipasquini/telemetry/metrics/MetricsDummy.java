package com.ipasquini.telemetry.metrics;

import com.ipasquini.telemetry.data.DataPoint;
import org.influxdb.InfluxDB;

/**
 * Influx implementation of the metrics service
 */
public class MetricsDummy implements Metrics {

	public void initialize() {

	}

	/**
	 * Push a {@link DataPoint} into {@link InfluxDB}
	 *
	 * @param dataPoint to be pushed
	 */
	public Void dataPoint(DataPoint dataPoint) {
		return null;
	}

	/**
	 * Push an error into {@link InfluxDB}
	 *
	 * @param kind of the error
	 */
	public Void error(String kind) {
		return null;
	}
}
