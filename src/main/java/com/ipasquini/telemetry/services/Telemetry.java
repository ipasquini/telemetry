package com.ipasquini.telemetry.services;

import com.ipasquini.telemetry.collectors.Collector;
import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.persistence.Persistence;
import org.msgpack.core.annotations.VisibleForTesting;
import org.springframework.stereotype.Service;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Manage {@link DataSource} and assign them a {@link Collector}
 */
@Service
public class Telemetry {

	private final Facade facade;
	private final Register register;
	private final Persistence persistence;
	private final List<Collector> collectors;
	private final DecoderFactory decoderFactory;

	public Telemetry(DecoderFactory decoderFactory, Register register, HealthMonitor healthMonitor, Persistence persistence,
					 Facade facade) {
		this.persistence = persistence;
		this.facade = facade;
		this.register = register;
		this.collectors = new ArrayList<>();
		this.decoderFactory = decoderFactory;
		initialize(healthMonitor);
	}

	@VisibleForTesting
	List<Collector> getCollectors() {
		return collectors;
	}

	/**
	 * Initialize the Telemetry service
	 */
	private void initialize(HealthMonitor healthMonitor) {
		loadDataSources();
		healthMonitor.checkEvery(collectors, 1000);
	}

	/**
	 * @return the active {@link List<DataSource>}
	 */
	public List<DataSource> getDataSources() {
		return collectors.stream().map(Collector::getDataSource).collect(Collectors.toList());
	}

	/**
	 * Add a new {@link DataSource} and start its corresponding {@link Collector}
	 *
	 * @param host     name of the {@link DataSource}
	 * @param port     number of the {@link DataSource}
	 * @param encoding of the {@link DataSource}
	 * @return the {@link DataSource} added
	 */
	public DataSource addDataSource(String host, short port, Encoding encoding) throws UnknownEncodingException,
			UnknownHostException {
		validateHost(host);

		DataSource dataSource = new DataSource(host, port, encoding);
		removeDataSource(dataSource);

		Collector collector = facade.getCollector(register, dataSource, decoderFactory, facade);
		collector.start();
		register.info(String.format("Started: %s", collector.getDataSource()));
		collectors.add(collector);

		persistence.insert(host, port, encoding);
		return dataSource;
	}

	/**
	 * @throws UnknownHostException if the host is not valid
	 */
	private void validateHost(String host) throws UnknownHostException {
		InetAddress.getByName(host);
	}

	/**
	 * Remove a {@link DataSource} and stop its corresponding {@link Collector}
	 *
	 * @param host     name of the {@link DataSource}
	 * @param port     number of the {@link DataSource}
	 * @param encoding of the {@link DataSource}
	 * @return the {@link DataSource} added
	 */
	public DataSource removeDataSource(String host, short port, Encoding encoding) {
		persistence.delete(host, port, encoding);
		DataSource dataSource = new DataSource(host, port, encoding);
		removeDataSource(dataSource);
		return dataSource;
	}

	private void removeDataSource(DataSource dataSource) {
		Optional<Collector> collectorOptional = collectors.stream()
				.filter(collector -> dataSource.equals(collector.getDataSource()))
				.findAny();

		collectorOptional.ifPresent(collector -> {
			collectors.remove(collector);
			collector.stop();
			register.info(String.format("Stopped: %s", collector.getDataSource()));
		});
	}

	/**
	 * Restore the active {@link List<DataSource>} from the persistent storage
	 */
	private void loadDataSources() {
		persistence.select().forEach(dataSource -> {
			try {
				Collector collector = facade.getCollector(register, dataSource, decoderFactory, facade);
				collector.start();
				collectors.add(collector);
				register.info(String.format("Started: %s", collector.getDataSource()));
			} catch (UnknownEncodingException e) {
				register.error("unknownencoding", e);
			}
		});
	}
}
