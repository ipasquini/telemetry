package com.ipasquini.telemetry.services;

import com.ipasquini.telemetry.collectors.Collector;
import com.ipasquini.telemetry.facade.Facade;
import org.msgpack.core.annotations.VisibleForTesting;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Monitor the health of {@link Collector}
 */
@Service
public class HealthMonitor {

	private final Facade facade;

	public HealthMonitor(Facade facade) {
		this.facade = facade;
	}

	/**
	 * Restart the broken {@link Collector} of the given {@link List} according to the specified period of time
	 *
	 * @param collectors to be monitored
	 * @param millis     that specify the period of time between each check
	 */
	public void checkEvery(List<Collector> collectors, long millis) {
		facade.getFuture(facade.getSupplier(() -> checkAsync(collectors, millis)));
	}

	/**
	 * Monitor the {@link List<Collector>} asynchronously
	 *
	 * @param collectors to be monitored
	 * @param millis     that specify the period of time between each check
	 */
	private Void checkAsync(List<Collector> collectors, long millis) {
		while (true) {
			restartBrokenCollectors(collectors);
			try {
				Thread.sleep(millis);
			} catch (InterruptedException ignored) {

			}
		}
	}

	/**
	 * Restart the broken {@link Collector} of the given {@link List}
	 *
	 * @param collectors to be monitored
	 */
	@VisibleForTesting
	void restartBrokenCollectors(List<Collector> collectors) {
		collectors
				.stream()
				.filter(collector -> !collector.getFuture().getNow(true))
				.forEach(Collector::start);
	}
}
