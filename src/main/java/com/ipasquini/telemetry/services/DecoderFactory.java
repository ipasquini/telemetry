package com.ipasquini.telemetry.services;

import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.decoders.BinaryDecoder;
import com.ipasquini.telemetry.decoders.Decoder;
import com.ipasquini.telemetry.decoders.StringDecoder;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import org.springframework.stereotype.Service;

/**
 * Build instances of {@link Decoder}
 */
@Service
public class DecoderFactory {

	/**
	 * Build a {@link Decoder} for thr given {@link Encoding}
	 *
	 * @param encoding of the data to decode
	 * @return a {@link Decoder} for the encoding
	 * @throws UnknownEncodingException encoding is not valid
	 */
	public Decoder build(Encoding encoding) throws UnknownEncodingException {
		if (Encoding.BINARY.equals(encoding)) {
			return new BinaryDecoder();
		}

		if (Encoding.STRING.equals(encoding)) {
			return new StringDecoder();
		}

		throw new UnknownEncodingException("Unknown encoding " + encoding.name());
	}
}
