package com.ipasquini.telemetry.services;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.metrics.Metrics;
import org.springframework.stereotype.Service;

/**
 * Register logs and metrics
 */
@Service
public class Register {

	private final Facade facade;
	private final Metrics metrics;

	public Register(Metrics metrics, Facade facade) {
		this.facade = facade;
		this.metrics = metrics;
		this.metrics.initialize();
	}

	/**
	 * Register a new {@link DataPoint}
	 *
	 * @param dataPoint to be registered
	 */
	public void dataPoint(DataPoint dataPoint) {
		try {
			metrics.dataPoint(dataPoint);
		} catch (Exception e) {
			facade.logError("Error: influxdb", e);
		}
	}

	/**
	 * Log a message
	 *
	 * @param message to be logged
	 */
	public void info(String message) {
		facade.logInfo(message);
	}

	/**
	 * Register a new error
	 *
	 * @param kind      of the error
	 * @param exception that caused the error
	 */
	public void error(String kind, Exception exception) {
		facade.logError(String.format("Error: %s", kind), exception);
		try {
			metrics.error(kind);
		} catch (Exception e) {
			facade.logError("Error: influxdb", e);
		}
	}
}
