package com.ipasquini.telemetry.persistence;

import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;

import java.util.List;

/**
 * Dummy implementation of {@link Persistence}
 */
public class PersistenceDummy implements Persistence {

	public List<DataSource> select() {
		return List.of();
	}

	public void insert(String host, short port, Encoding encoding) {

	}

	public void delete(String host, short port, Encoding encoding) {

	}
}
