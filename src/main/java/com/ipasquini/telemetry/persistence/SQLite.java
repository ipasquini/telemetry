package com.ipasquini.telemetry.persistence;

import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.stream.Collectors;

/**
 * SQLite implementation of {@link Persistence}
 */
public class SQLite implements Persistence {

	private static final String SELECT = "SELECT host, port, encoding FROM datasource";
	private static final String INSERT = "INSERT INTO datasource(host, port, encoding) VALUES ('%s', %s, '%s')";
	private static final String DELETE = "DELETE FROM datasource WHERE host = '%s' AND port = %s AND encoding = '%s'";
	private static final String CREATE = "CREATE TABLE IF NOT EXISTS datasource(host TEXT, port INTEGER, encoding TEXT)";

	private final JdbcTemplate jdbcTemplate;

	public SQLite(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		jdbcTemplate.execute(CREATE);
	}

	/**
	 * Retrieve the stored {@link DataSource}
	 *
	 * @return the stored {@link List<DataSource>}
	 */
	public List<DataSource> select() {
		return jdbcTemplate
				.queryForList(SELECT)
				.stream()
				.map(map -> {
					String host = map.get("host").toString();
					short port = Short.parseShort(map.get("port").toString());
					Encoding encoding = Encoding.valueOf(map.get("encoding").toString());
					return new DataSource(host, port, encoding);
				})
				.collect(Collectors.toList());
	}

	/**
	 * Insert a new {@link DataSource}
	 *
	 * @param host     name of the {@link DataSource} to store
	 * @param port     number of the {@link DataSource} to store
	 * @param encoding of the {@link DataSource} to store
	 */
	public void insert(String host, short port, Encoding encoding) {
		jdbcTemplate.execute(String.format(INSERT, host, port, encoding));
	}

	/**
	 * Delete a stored {@link DataSource}
	 *
	 * @param host     name of the {@link DataSource} to delete
	 * @param port     number of the {@link DataSource} to delete
	 * @param encoding of the {@link DataSource} to delete
	 */
	public void delete(String host, short port, Encoding encoding) {
		jdbcTemplate.execute(String.format(DELETE, host, port, encoding));
	}
}
