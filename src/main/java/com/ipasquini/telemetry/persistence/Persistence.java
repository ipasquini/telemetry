package com.ipasquini.telemetry.persistence;

import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;

import java.util.List;

/**
 * Interface for {@link DataSource} persistence
 */
public interface Persistence {

	List<DataSource> select();

	void insert(String host, short port, Encoding encoding);

	void delete(String host, short port, Encoding encoding);
}
