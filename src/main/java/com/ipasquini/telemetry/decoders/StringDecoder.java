package com.ipasquini.telemetry.decoders;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.exceptions.MalformedInputException;

import java.util.regex.Pattern;

/**
 * Decodes string data
 */
public class StringDecoder implements Decoder {

	private static final Pattern PATTERN = Pattern.compile("^\\[[0-9]+:[0-9]+:[-]?[0-9]+[.][0-9]+\\]$");

	@Override
	public DataPoint decode(byte[] buffer, int read) throws MalformedInputException {
		String raw = new String(buffer, 0, read);
		checkFormat(raw);
		String[] data = raw.substring(1, raw.length() - 1).split(":");
		long timestamp = Long.parseLong(data[0]);
		short id = Short.parseShort(data[1]);
		float value = Float.parseFloat(data[2]);
		return new DataPoint(timestamp, id, value);
	}

	/**
	 * @throws MalformedInputException if the raw data format is not valid
	 */
	private void checkFormat(String raw) throws MalformedInputException {
		if (!PATTERN.matcher(raw).find()) {
			throw new MalformedInputException("Input is not legal");
		}
	}
}
