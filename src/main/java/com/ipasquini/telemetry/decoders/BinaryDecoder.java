package com.ipasquini.telemetry.decoders;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.exceptions.MalformedInputException;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/**
 * Decodes binary data
 */
public class BinaryDecoder implements Decoder {

	@Override
	public DataPoint decode(byte[] buffer, int read) throws MalformedInputException {
		checkHeader(buffer);

		ByteBuffer byteBuffer = ByteBuffer.wrap(Arrays.copyOfRange(buffer, 4, 12));
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		long timestamp = byteBuffer.getLong();

		byteBuffer = ByteBuffer.wrap(Arrays.copyOfRange(buffer, 12, 14));
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		short id = byteBuffer.getShort();

		byteBuffer = ByteBuffer.wrap(Arrays.copyOfRange(buffer, 14, 18));
		byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
		float value = byteBuffer.getFloat();

		return new DataPoint(timestamp, id, value);
	}

	/**
	 * @throws MalformedInputException if the header of the raw data is not valid
	 */
	private void checkHeader(byte[] buffer) throws MalformedInputException {
		if (buffer[0] != 0 || buffer[1] != 1 || buffer[2] != 2 || buffer[3] != 3) {
			throw new MalformedInputException("Input header is not legal");
		}
	}
}
