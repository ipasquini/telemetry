package com.ipasquini.telemetry.decoders;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.exceptions.MalformedInputException;

/**
 * Decodes raw data
 */
public interface Decoder {

	/**
	 * Transforms raw data into {@link DataPoint}
	 *
	 * @param buffer into which the data is read
	 * @param read   is the total number of bytes read into the buffer
	 * @return a {@link DataPoint} that represents the raw data
	 * @throws MalformedInputException if the raw data is not valid
	 */
	DataPoint decode(byte[] buffer, int read) throws MalformedInputException;

}
