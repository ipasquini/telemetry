package com.ipasquini.telemetry.exceptions;

/**
 * Exception thrown when host is not valid
 */
public class UnknownEncodingException extends Exception {

	public UnknownEncodingException(String errorMessage) {
		super(errorMessage);
	}
}
