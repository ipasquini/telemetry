package com.ipasquini.telemetry.exceptions;

/**
 * Exception thrown when raw data is not valid
 */
public class MalformedInputException extends Exception {

	public MalformedInputException(String errorMessage) {
		super(errorMessage);
	}
}
