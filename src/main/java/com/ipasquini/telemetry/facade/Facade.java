package com.ipasquini.telemetry.facade;

import com.ipasquini.telemetry.collectors.Collector;
import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.services.DecoderFactory;
import com.ipasquini.telemetry.services.Register;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Interface that wraps the creation of new objects and uses of static methods to allow mock them in testing
 */
public interface Facade {

	<U> CompletableFuture<U> getFuture(Supplier<U> supplier);

	Socket getSocket(String host, int port) throws IOException;

	<U> Supplier<U> getSupplier(Supplier<U> supplier);

	String getJson(String key, Object value);

	InfluxDB getInfluxDB(String url, String username, String password);

	Point buildPoint(String measurement, Long timeToSet, TimeUnit timeUnit, String tagName, String tagValue,
					 String fieldName, int fieldValue);

	Point buildPoint(String measurement, Long timeToSet, TimeUnit timeUnit, String tagName, String tagValue,
					 String fieldName, float fieldValue);

	void logInfo(String message);

	void logError(String message, Exception exception);

	Collector getCollector(Register register, DataSource dataSource, DecoderFactory decoderFactory, Facade facade)
			throws UnknownEncodingException;
}
