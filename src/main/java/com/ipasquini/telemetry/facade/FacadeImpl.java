package com.ipasquini.telemetry.facade;

import com.ipasquini.telemetry.TelemetryApplication;
import com.ipasquini.telemetry.collectors.Collector;
import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.services.DecoderFactory;
import com.ipasquini.telemetry.services.Register;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Productive implementation of {@link Facade}
 */
public class FacadeImpl implements Facade {

	static final Logger LOGGER = LoggerFactory.getLogger(TelemetryApplication.class);

	/**
	 * Create a new {@link CompletableFuture}
	 *
	 * @param supplier a function returning the value to be used
	 * @return a new {@link CompletableFuture}
	 */
	public <U> CompletableFuture<U> getFuture(Supplier<U> supplier) {
		return CompletableFuture.supplyAsync(supplier);
	}

	/**
	 * Create a new {@link Socket}
	 *
	 * @param host name
	 * @param port number
	 * @return a new {@link Socket}
	 * @throws IOException if an I/O error occurs when creating the socket
	 */
	public Socket getSocket(String host, int port) throws IOException {
		return new Socket(host, port);
	}

	/**
	 * @param supplier a function returning the value to be used
	 * @return the same {@link Supplier<U>}
	 */
	public <U> Supplier<U> getSupplier(Supplier<U> supplier) {
		return supplier;
	}

	/**
	 * @param key   of the json field
	 * @param value of the json field
	 * @return a {@link String} that represents key and value in JSON format
	 */
	public String getJson(String key, Object value) {
		return new JSONObject().put(key, value).toString().replace("\\", "");
	}

	/**
	 * Create a new {@link InfluxDB}
	 *
	 * @param url      of the database
	 * @param username of the database
	 * @param password of the database
	 * @return a new {@link InfluxDB}
	 */
	public InfluxDB getInfluxDB(String url, String username, String password) {
		return InfluxDBFactory.connect(url, username, password);
	}

	/**
	 * Create a new {@link Point}
	 *
	 * @param measurement name
	 * @param timeToSet   of the point
	 * @param timeUnit    of the given timeToSet
	 * @param tagName     is the name of the tag
	 * @param tagValue    is the value of the tag
	 * @param fieldName   is the name of the field
	 * @param fieldValue  is the value of the field
	 * @return a new {@link Point}
	 */
	public Point buildPoint(String measurement, Long timeToSet, TimeUnit timeUnit, String tagName, String tagValue,
							String fieldName, int fieldValue) {
		return Point.measurement(measurement)
				.time(timeToSet, timeUnit)
				.tag(tagName, tagValue)
				.addField(fieldName, fieldValue)
				.build();
	}

	/**
	 * Create a new {@link Point}
	 *
	 * @param measurement name
	 * @param timeToSet   of the point
	 * @param timeUnit    of the given timeToSet
	 * @param tagName     is the name of the tag
	 * @param tagValue    is the value of the tag
	 * @param fieldName   is the name of the field
	 * @param fieldValue  is the value of the field
	 * @return a new {@link Point}
	 */
	public Point buildPoint(String measurement, Long timeToSet, TimeUnit timeUnit, String tagName, String tagValue,
							String fieldName, float fieldValue) {
		return Point.measurement(measurement)
				.time(timeToSet, timeUnit)
				.tag(tagName, tagValue)
				.addField(fieldName, fieldValue)
				.build();
	}

	/**
	 * Log an informative message
	 *
	 * @param message to be logged
	 */
	public void logInfo(String message) {
		LOGGER.info(message);
	}

	/**
	 * Log an informative message
	 *
	 * @param message   of the error to be logged
	 * @param exception that caused the error
	 */
	public void logError(String message, Exception exception) {
		LOGGER.error(message, exception);
	}

	/**
	 * Create a new {@link Collector}
	 *
	 * @param register       used by {@link Collector}
	 * @param dataSource     of the {@link Collector}
	 * @param decoderFactory used by {@link Collector}
	 * @param facade         used by {@link Collector}
	 * @return a new {@link Collector}
	 */
	public Collector getCollector(Register register, DataSource dataSource, DecoderFactory decoderFactory,
								  Facade facade) throws UnknownEncodingException {
		return new Collector(register, dataSource, decoderFactory, facade);
	}
}
