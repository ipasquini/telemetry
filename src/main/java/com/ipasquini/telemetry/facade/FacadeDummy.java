package com.ipasquini.telemetry.facade;

import com.ipasquini.telemetry.collectors.Collector;
import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.services.DecoderFactory;
import com.ipasquini.telemetry.services.Register;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Dummy implementation of {@link Facade}
 */
public class FacadeDummy implements Facade {

	public <U> CompletableFuture<U> getFuture(Supplier<U> supplier) {
		return CompletableFuture.supplyAsync(supplier);
	}

	public Socket getSocket(String host, int port) throws IOException {
		return new Socket(host, port);
	}

	public <U> Supplier<U> getSupplier(Supplier<U> supplier) {
		return supplier;
	}

	public String getJson(String key, Object value) {
		return new JSONObject().put(key, value).toString().replace("\\", "");
	}

	public InfluxDB getInfluxDB(String url, String username, String password) {
		return null;
	}

	public Point buildPoint(String measurement, Long timeToSet, TimeUnit timeUnit, String tagName, String tagValue,
							String fieldName, int fieldValue) {
		return null;
	}

	public Point buildPoint(String measurement, Long timeToSet, TimeUnit timeUnit, String tagName, String tagValue,
							String fieldName, float fieldValue) {
		return null;
	}

	public void logInfo(String message) {

	}

	public void logError(String message, Exception exception) {

	}

	public Collector getCollector(Register register, DataSource dataSource, DecoderFactory decoderFactory,
								  Facade facade) throws UnknownEncodingException {
		return new Collector(register, dataSource, decoderFactory, facade);
	}
}
