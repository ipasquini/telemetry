package com.ipasquini.telemetry.data;

/**
 * Represents a sample measured by a satellite
 */
public class DataPoint {

	private final long timestamp;
	private final short id;
	private final float value;

	public DataPoint(long timestamp, short id, float value) {
		this.timestamp = timestamp;
		this.id = id;
		this.value = value;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public short getId() {
		return id;
	}

	public float getValue() {
		return value;
	}
}
