package com.ipasquini.telemetry.data;

import org.json.JSONObject;

import java.util.Objects;

/**
 * Represents a TCP server of a satellite
 */
public class DataSource {

	private final String host;
	private final short port;
	private final Encoding encoding;

	public DataSource(String host, short port, Encoding encoding) {
		this.host = host;
		this.port = port;
		this.encoding = encoding;
	}

	public String getHost() {
		return host;
	}

	public short getPort() {
		return port;
	}

	public Encoding getEncoding() {
		return encoding;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		DataSource that = (DataSource) o;
		return port == that.port &&
				host.equals(that.host) &&
				encoding == that.encoding;
	}

	@Override
	public int hashCode() {
		return Objects.hash(host, port, encoding);
	}

	@Override
	public String toString() {
		return new JSONObject()
				.put("host", host)
				.put("port", port)
				.put("encoding", encoding)
				.toString();
	}
}
