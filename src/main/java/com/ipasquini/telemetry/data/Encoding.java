package com.ipasquini.telemetry.data;

/**
 * Data encoding type
 */
public enum Encoding {
	BINARY,
	STRING
}
