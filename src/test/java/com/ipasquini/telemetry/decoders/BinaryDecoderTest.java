package com.ipasquini.telemetry.decoders;

import static org.junit.Assert.assertEquals;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.exceptions.MalformedInputException;
import org.junit.Before;
import org.junit.Test;

public class BinaryDecoderTest {

	private static final int READ = 18;

	private BinaryDecoder binaryDecoder;

	@Before
	public void setUp() {
		binaryDecoder = new BinaryDecoder();
	}

	@Test(expected = MalformedInputException.class)
	public void testDecodeWhenHeaderFirstByteIsNotValid() throws MalformedInputException {
		byte[] buffer = getBuffer();
		buffer[0] = 9;
		binaryDecoder.decode(buffer, READ);
	}

	@Test(expected = MalformedInputException.class)
	public void testDecodeWhenHeaderSecondByteIsNotValid() throws MalformedInputException {
		byte[] buffer = getBuffer();
		buffer[1] = 9;
		binaryDecoder.decode(buffer, READ);
	}

	@Test(expected = MalformedInputException.class)
	public void testDecodeWhenHeaderThirdByteIsNotValid() throws MalformedInputException {
		byte[] buffer = getBuffer();
		buffer[2] = 9;
		binaryDecoder.decode(buffer, READ);
	}

	@Test(expected = MalformedInputException.class)
	public void testDecodeWhenHeaderFourthByteIsNotValid() throws MalformedInputException {
		byte[] buffer = getBuffer();
		buffer[3] = 9;
		binaryDecoder.decode(buffer, READ);
	}

	@Test
	public void testDecodeWhenHeaderIsValid() throws MalformedInputException {
		byte[] buffer = getBuffer();
		DataPoint decoded = binaryDecoder.decode(buffer, READ);
		assertEquals(1604873441, decoded.getTimestamp());
		assertEquals(3, decoded.getId());
		assertEquals(-1056.16650390625, decoded.getValue(), 0);
	}

	private byte[] getBuffer() {
		byte[] buffer = new byte[1024];
		buffer[0] = 0;
		buffer[1] = 1;
		buffer[2] = 2;
		buffer[3] = 3;
		buffer[4] = -31;
		buffer[5] = 108;
		buffer[6] = -88;
		buffer[7] = 95;
		buffer[8] = 0;
		buffer[9] = 0;
		buffer[10] = 0;
		buffer[11] = 0;
		buffer[12] = 3;
		buffer[13] = 0;
		buffer[14] = 84;
		buffer[15] = 5;
		buffer[16] = -124;
		buffer[17] = -60;
		buffer[18] = 0;
		return buffer;
	}
}
