package com.ipasquini.telemetry.decoders;

import static org.junit.Assert.assertEquals;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.exceptions.MalformedInputException;
import org.junit.Before;
import org.junit.Test;

public class StringDecoderTest {

	private static final int READ = 26;

	private StringDecoder stringDecoder;

	@Before
	public void setUp() {
		stringDecoder = new StringDecoder();
	}

	@Test(expected = MalformedInputException.class)
	public void decodeWhenFormatIsInvalid() throws MalformedInputException {
		byte[] buffer = getBuffer();
		buffer[0] = 0;
		stringDecoder.decode(buffer, READ);
	}

	@Test
	public void decodeWhenFormatIsValid() throws MalformedInputException {
		byte[] buffer = getBuffer();
		DataPoint decoded = stringDecoder.decode(buffer, READ);
		assertEquals(1604873885, decoded.getTimestamp());
		assertEquals(1, decoded.getId());
		assertEquals(-174.23597717285156, decoded.getValue(), 0);
	}

	private byte[] getBuffer() {
		byte[] buffer = new byte[1024];
		buffer[0] = 91;
		buffer[1] = 49;
		buffer[2] = 54;
		buffer[3] = 48;
		buffer[4] = 52;
		buffer[5] = 56;
		buffer[6] = 55;
		buffer[7] = 51;
		buffer[8] = 56;
		buffer[9] = 56;
		buffer[10] = 53;
		buffer[11] = 58;
		buffer[12] = 49;
		buffer[13] = 58;
		buffer[14] = 45;
		buffer[15] = 49;
		buffer[16] = 55;
		buffer[17] = 52;
		buffer[18] = 46;
		buffer[19] = 50;
		buffer[20] = 51;
		buffer[21] = 53;
		buffer[22] = 57;
		buffer[23] = 55;
		buffer[24] = 55;
		buffer[25] = 93;
		return buffer;
	}
}
