package com.ipasquini.telemetry.collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.decoders.Decoder;
import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.exceptions.MalformedInputException;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.services.DecoderFactory;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.services.Register;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class CollectorTest {

	private static final int READ = 123;
	private static final short PORT = 321;
	private static final String HOST = "1.2.3.4";
	private static final byte[] BUFFER = new byte[0];
	private static final Supplier<Object> SUPPLIER = () -> Boolean.TRUE;
	private static final DataPoint DATA_POINT = new DataPoint(3L, (short) 4, 5.6f);
	private static final CompletableFuture<Object> FUTURE = CompletableFuture.supplyAsync(SUPPLIER);

	@Mock
	private Facade facade;

	@Mock
	private Socket socket;

	@Mock
	private Decoder decoder;

	@Mock
	private Register register;

	@Mock
	private InputStream inputStream;

	@Mock
	private DecoderFactory decoderFactory;

	private Collector collector;
	private DataSource dataSource;

	@Before
	public void setUp() throws UnknownEncodingException {
		MockitoAnnotations.initMocks(this);
		when(decoderFactory.build(Encoding.BINARY)).thenReturn(decoder);
		dataSource = new DataSource(HOST, PORT, Encoding.BINARY);
		collector = new Collector(register, dataSource, decoderFactory, facade);
	}

	@Test
	public void testStart() {
		when(facade.getSupplier(any())).thenReturn(SUPPLIER);
		when(facade.getFuture(SUPPLIER)).thenReturn(FUTURE);
		collector.start();
		assertEquals(FUTURE, collector.getFuture());
	}

	@Test
	public void testStop() {
		assertTrue(collector.getRunning());
		collector.stop();
		assertFalse(collector.getRunning());
	}

	@Test
	public void testCollectAsyncWhenLostConnection() throws IOException {
		when(facade.getSocket(HOST, PORT)).thenReturn(socket);
		when(socket.getInputStream()).thenReturn(inputStream);
		when(inputStream.read(any())).thenReturn(-1);

		assertFalse(collector.collectAsync(dataSource));
		verify(register).error("lostconnection", null);
		verifyNoMoreInteractions(register);
	}

	@Test
	public void testCollectAsyncWhenIOException() throws IOException {
		IOException ioException = new IOException("FAKE EXCEPTION");
		when(facade.getSocket(HOST, PORT)).thenThrow(ioException);

		assertFalse(collector.collectAsync(dataSource));
		verify(register).error("ioexception", ioException);
		verifyNoMoreInteractions(register);
	}

	@Test
	public void testCollectAsyncWhenUnknownError() throws IOException {
		RuntimeException runtimeException = new RuntimeException("FAKE EXCEPTION");
		when(facade.getSocket(HOST, PORT)).thenThrow(runtimeException);

		assertFalse(collector.collectAsync(dataSource));
		verify(register).error("unknownerror", runtimeException);
		verifyNoMoreInteractions(register);
	}

	@Test
	public void testCollectAsyncWhenIsStopped() throws IOException {
		when(facade.getSocket(HOST, PORT)).thenReturn(socket);
		when(socket.getInputStream()).thenReturn(inputStream);
		when(inputStream.read(any())).thenReturn(1);
		collector.stop();

		assertTrue(collector.collectAsync(dataSource));
		verifyNoMoreInteractions(register);
	}

	@Test
	public void processDataWhenInputIsOK() throws MalformedInputException {
		when(decoder.decode(BUFFER, READ)).thenReturn(DATA_POINT);

		collector.processData(BUFFER, READ);
		verify(register).dataPoint(DATA_POINT);
		verifyNoMoreInteractions(register);
	}

	@Test
	public void processDataWhenMalformedInput() throws MalformedInputException {
		MalformedInputException malformedInputException = new MalformedInputException("FAKE EXCEPTION");
		when(decoder.decode(BUFFER, READ)).thenThrow(malformedInputException);

		collector.processData(BUFFER, READ);
		verify(register).error("malformedInput", malformedInputException);
		verifyNoMoreInteractions(register);
	}
}
