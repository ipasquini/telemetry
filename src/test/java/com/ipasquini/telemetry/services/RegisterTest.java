package com.ipasquini.telemetry.services;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.metrics.Metrics;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class RegisterTest {

	private static final String KIND = "kind";
	private static final String MESSAGE = "message";
	private static final Exception EXCEPTION = new Exception("FAKE EXCEPTION");
	private static final DataPoint DATA_POINT = new DataPoint(3L, (short) 4, 5.6f);

	@Mock
	private Metrics metrics;

	@Mock
	private Facade facade;

	private Register register;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		register = new Register(metrics, facade);
	}

	@Test
	public void testDataPointWithInfluxError() {
		RuntimeException runtimeException = new RuntimeException("FAKE EXCEPTION");
		when(metrics.dataPoint(DATA_POINT)).thenThrow(runtimeException);
		register.dataPoint(DATA_POINT);
		verify(facade).logError("Error: influxdb", runtimeException);
		verifyNoMoreInteractions(facade);
	}

	@Test
	public void testDataPointWithoutInfluxError() {
		register.dataPoint(DATA_POINT);
		verify(metrics).dataPoint(DATA_POINT);
		verifyNoMoreInteractions(facade);
	}

	@Test
	public void testInfo() {
		register.info(MESSAGE);
		verify(facade).logInfo(MESSAGE);
		verifyNoMoreInteractions(facade);
	}

	@Test
	public void testErrorWithoInfluxError() {
		RuntimeException runtimeException = new RuntimeException("FAKE EXCEPTION");
		when(metrics.error(KIND)).thenThrow(runtimeException);
		register.error(KIND, EXCEPTION);
		verify(facade).logError(String.format("Error: %s", KIND), EXCEPTION);
		verify(facade).logError("Error: influxdb", runtimeException);
		verifyNoMoreInteractions(facade);
	}

	@Test
	public void testErrorWithoutInfluxError() {
		register.error(KIND, EXCEPTION);
		verify(facade).logError(String.format("Error: %s", KIND), EXCEPTION);
		verify(metrics).error(KIND);
		verifyNoMoreInteractions(facade);
	}
}
