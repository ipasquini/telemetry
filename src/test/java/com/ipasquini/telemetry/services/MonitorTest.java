package com.ipasquini.telemetry.services;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ipasquini.telemetry.collectors.Collector;
import com.ipasquini.telemetry.facade.Facade;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class MonitorTest {

	private static final long MILLIS = 1000;
	private static final Supplier<Object> SUPPLIER = () -> Boolean.TRUE;

	@Mock
	private Facade facade;

	@Mock
	private Collector collector;

	@Mock
	private CompletableFuture<Boolean> future;

	private HealthMonitor healthMonitor;
	private List<Collector> collectors;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		healthMonitor = new HealthMonitor(facade);
		collectors = List.of(collector);
	}

	@Test
	public void testCheckEvery() {
		when(facade.getSupplier(any())).thenReturn(SUPPLIER);
		healthMonitor.checkEvery(collectors, MILLIS);
		verify(facade).getFuture(SUPPLIER);
	}

	@Test
	public void testRestartBrokenCollectorsWhenIsBroken() {
		when(collector.getFuture()).thenReturn(future);
		when(future.getNow(true)).thenReturn(false);
		healthMonitor.restartBrokenCollectors(collectors);
		verify(collector).start();
	}

	@Test
	public void testRestartBrokenCollectorsWhenIsNotBroken() {
		when(collector.getFuture()).thenReturn(future);
		when(future.getNow(true)).thenReturn(true);
		healthMonitor.restartBrokenCollectors(collectors);
		verify(collector, never()).start();
	}
}
