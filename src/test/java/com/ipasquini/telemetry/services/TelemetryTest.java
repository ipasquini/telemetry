package com.ipasquini.telemetry.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ipasquini.telemetry.collectors.Collector;
import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.persistence.Persistence;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.net.UnknownHostException;
import java.util.List;

public class TelemetryTest {

	private static final String HOST = "1.2.3.4";
	private static final short PORT = (short) 1000;
	private static final short PORT_2 = (short) 1002;
	private static final Encoding ENCODING = Encoding.BINARY;
	private static final DataSource DATA_SOURCE_1 = new DataSource("1.2.3.4", (short) 1000, Encoding.BINARY);
	private static final DataSource DATA_SOURCE_2 = new DataSource("1.2.3.4", (short) 1002, Encoding.BINARY);

	@Mock
	private Facade facade;

	@Mock
	private Register register;

	@Mock
	private Persistence persistence;

	@Mock
	private HealthMonitor healthMonitor;

	@Mock
	private Collector collector1, collector2;

	@Mock
	private DecoderFactory decoderFactory;

	private Telemetry telemetry;

	@Before
	public void setUp() throws UnknownEncodingException {
		MockitoAnnotations.initMocks(this);
		when(persistence.select()).thenReturn(List.of(DATA_SOURCE_1));
		when(facade.getCollector(register, DATA_SOURCE_1, decoderFactory, facade)).thenReturn(collector1);
		telemetry = new Telemetry(decoderFactory, register, healthMonitor, persistence, facade);
	}

	@Test
	public void testInitialize() {
		verify(collector1).start();
		verify(healthMonitor).checkEvery(telemetry.getCollectors(), 1000);
		assertEquals(1, telemetry.getCollectors().size());
		assertEquals(collector1, telemetry.getCollectors().get(0));
	}

	@Test
	public void testGetDataSources() {
		when(collector1.getDataSource()).thenReturn(DATA_SOURCE_1);
		assertEquals(List.of(DATA_SOURCE_1), telemetry.getDataSources());
	}

	@Test
	public void testAddDataSourceWithHostIsValid() throws UnknownEncodingException, UnknownHostException {
		when(facade.getCollector(register, DATA_SOURCE_2, decoderFactory, facade)).thenReturn(collector2);
		assertEquals(DATA_SOURCE_2, telemetry.addDataSource(HOST, PORT_2, ENCODING));
		assertEquals(2, telemetry.getCollectors().size());
		verify(collector2).start();
		verify(persistence).insert(HOST, PORT_2, ENCODING);
	}

	@Test(expected = UnknownHostException.class)
	public void testAddDataSourceWithHostIsNotValid() throws UnknownEncodingException, UnknownHostException {
		telemetry.addDataSource("not valid", PORT, ENCODING);
	}

	@Test
	public void testRemoveDataSource() {
		when(collector1.getDataSource()).thenReturn(DATA_SOURCE_1);
		assertEquals(DATA_SOURCE_1, telemetry.removeDataSource(HOST, PORT, ENCODING));
		assertTrue(telemetry.getCollectors().isEmpty());
		verify(persistence).delete(HOST, PORT, ENCODING);
		verify(collector1).stop();
	}
}
