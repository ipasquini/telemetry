package com.ipasquini.telemetry.services;

import static org.junit.Assert.assertEquals;

import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.decoders.BinaryDecoder;
import com.ipasquini.telemetry.decoders.StringDecoder;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import org.junit.Before;
import org.junit.Test;

public class DecoderFactoryTest {

	private DecoderFactory decoderFactory;

	@Before
	public void setUp() {
		decoderFactory = new DecoderFactory();
	}

	@Test
	public void testBuildWhenEncodingIsBinary() throws UnknownEncodingException {
		assertEquals(BinaryDecoder.class, decoderFactory.build(Encoding.BINARY).getClass());
	}

	@Test
	public void testBuildWhenEncodingIsString() throws UnknownEncodingException {
		assertEquals(StringDecoder.class, decoderFactory.build(Encoding.STRING).getClass());
	}
}
