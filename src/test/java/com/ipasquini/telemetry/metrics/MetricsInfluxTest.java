package com.ipasquini.telemetry.metrics;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import com.ipasquini.telemetry.data.DataPoint;
import com.ipasquini.telemetry.facade.Facade;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.TimeUnit;

public class MetricsInfluxTest {

	private static final String ID = "id";
	private static final String URL = "url";
	private static final String KIND = "kind";
	private static final String VALUE = "value";
	private static final String ERROR = "error";
	private static final String COUNT = "count";
	private static final String PASSWORD = "password";
	private static final String DATABASE = "database";
	private static final String USERNAME = "user name";
	private static final String DATAPOINT = "datapoint";
	private static final String ERROR_KIND = "an error kind";
	private static final String CREATE = "CREATE DATABASE %s";
	private static final DataPoint DATA_POINT = new DataPoint(3L, (short) 4, 5.6f);

	@Mock
	private Point point;

	@Mock
	private Facade facade;

	@Mock
	private InfluxDB influxDB;

	private MetricsInflux metrics;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		when(facade.getInfluxDB(URL, USERNAME, PASSWORD)).thenReturn(influxDB);
		metrics = new MetricsInflux(facade);
		metrics.setLocalURL(URL);
		metrics.setUsername(USERNAME);
		metrics.setPassword(PASSWORD);
		metrics.setDatabase(DATABASE);
	}

	@Test
	public void testInitialize() {
		metrics.initialize();
		verify(influxDB).query(new Query(String.format(CREATE, DATABASE)));
		verify(influxDB).setDatabase(DATABASE);
		verifyNoMoreInteractions(influxDB);
	}

	@Test
	public void testDataPoint() {
		when(facade.buildPoint(
				DATAPOINT,
				3L,
				TimeUnit.SECONDS, ID,
				"4",
				VALUE,
				5.6f))
				.thenReturn(point);

		metrics.initialize();
		metrics.dataPoint(DATA_POINT);
		verify(influxDB).write(point);
	}

	@Test
	public void testError() {
		when(facade.buildPoint(
				eq(ERROR),
				anyLong(),
				eq(TimeUnit.MILLISECONDS),
				eq(KIND),
				eq(ERROR_KIND),
				eq(COUNT),
				eq(1)))
				.thenReturn(point);

		metrics.initialize();
		metrics.error(ERROR_KIND);
		verify(influxDB).write(point);
	}
}
