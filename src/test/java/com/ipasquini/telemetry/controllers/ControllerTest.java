package com.ipasquini.telemetry.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import com.ipasquini.telemetry.facade.Facade;
import com.ipasquini.telemetry.services.Register;
import com.ipasquini.telemetry.services.Telemetry;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.server.ResponseStatusException;

import java.net.UnknownHostException;
import java.util.List;

public class ControllerTest {

	private static final short PORT = 321;
	private static final String HOST = "1.2.3.4";
	private static final String JSON = "{\"a\", \"b\"}";
	private static final Encoding TYPE = Encoding.BINARY;
	private static final DataSource DATA_SOURCE = new DataSource("1.2.3.4", (short) 5, Encoding.BINARY);
	private static final List<DataSource> DATA_SOURCE_LIST = List.of(DATA_SOURCE);

	@Mock
	private Facade facade;

	@Mock
	private Register register;

	@Mock
	private Telemetry telemetry;

	private Controller controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		controller = new Controller(telemetry, register, facade);
	}

	@Test
	public void testGetDataSources() {
		when(telemetry.getDataSources()).thenReturn(DATA_SOURCE_LIST);
		when(facade.getJson("datasources", DATA_SOURCE_LIST)).thenReturn(JSON);
		assertEquals(JSON, controller.getDataSources());
	}

	@Test
	public void testAddDataSourceWhenHostIsOK() throws UnknownEncodingException, UnknownHostException {
		when(telemetry.addDataSource(HOST, PORT, TYPE)).thenReturn(DATA_SOURCE);
		when(facade.getJson("added", DATA_SOURCE)).thenReturn(JSON);
		assertEquals(JSON, controller.addDataSource(HOST, PORT, TYPE));
	}

	@Test(expected = ResponseStatusException.class)
	public void testAddDataSourceWhenUnknownHost() throws UnknownEncodingException, UnknownHostException {
		when(telemetry.addDataSource(HOST, PORT, TYPE)).thenThrow(new UnknownHostException("FAKE EXCEPTION"));
		controller.addDataSource(HOST, PORT, TYPE);
	}

	@Test
	public void testRemoveDataSource() {
		when(telemetry.removeDataSource(HOST, PORT, TYPE)).thenReturn(DATA_SOURCE);
		when(facade.getJson("removed", DATA_SOURCE)).thenReturn(JSON);
		assertEquals(JSON, controller.removeDataSource(HOST, PORT, TYPE));
	}
}
