package com.ipasquini.telemetry.persistence;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.ipasquini.telemetry.data.DataSource;
import com.ipasquini.telemetry.data.Encoding;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;
import java.util.Map;

public class SQLiteTest {

	private static final String PORT = "1000";
	private static final String HOST = "1.2.3.4";
	private static final String ENCODING = "BINARY";
	private static final String SELECT = "SELECT host, port, encoding FROM datasource";
	private static final String CREATE = "CREATE TABLE IF NOT EXISTS datasource(host TEXT, port INTEGER, encoding TEXT)";
	private static final String INSERT = "INSERT INTO datasource(host, port, encoding) VALUES ('1.2.3.4', 1000, 'BINARY')";
	private static final String DELETE = "DELETE FROM datasource WHERE host = '1.2.3.4' AND port = 1000 AND encoding = 'BINARY'";
	private static final List<Map<String, Object>> STORED = List.of(Map.of(
			"host", HOST,
			"port", PORT,
			"encoding", ENCODING));

	@Mock
	private JdbcTemplate jdbcTemplate;

	private Persistence persistence;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		persistence = new SQLite(jdbcTemplate);
		verify(jdbcTemplate).execute(CREATE);
	}

	@Test
	public void testSelect() {
		when(jdbcTemplate.queryForList(SELECT)).thenReturn(STORED);
		List<DataSource> result = persistence.select();
		assertEquals(1, result.size());
		DataSource dataSource = result.get(0);
		assertEquals(HOST, dataSource.getHost());
		assertEquals(Short.parseShort(PORT), dataSource.getPort());
		assertEquals(Encoding.BINARY, dataSource.getEncoding());
	}

	@Test
	public void testInsert() {
		persistence.insert(HOST, Short.parseShort(PORT), Encoding.BINARY);
		verify(jdbcTemplate).execute(INSERT);
	}

	@Test
	public void testDelete() {
		persistence.delete(HOST, Short.parseShort(PORT), Encoding.BINARY);
		verify(jdbcTemplate).execute(DELETE);
	}
}
