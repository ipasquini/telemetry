package com.ipasquini.telemetry;

import static org.junit.Assert.assertEquals;

import com.ipasquini.telemetry.controllers.Controller;
import com.ipasquini.telemetry.data.Encoding;
import com.ipasquini.telemetry.exceptions.UnknownEncodingException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class EndToEndTest {

	private static final short PORT_1 = 1001;
	private static final short PORT_2 = 1002;
	private static final String HOST_1 = "127.0.0.1";
	private static final String HOST_2 = "localhost";
	private static final Encoding ENCODING_1 = Encoding.BINARY;
	private static final Encoding ENCODING_2 = Encoding.STRING;

	@Autowired
	private ApplicationContext context;

	@Test
	void contextLoads() throws UnknownEncodingException {
		Controller controller = context.getBean(Controller.class);
		assertEquals("{\"datasources\":\"[]\"}", controller.getDataSources());

		controller.addDataSource(HOST_1, PORT_1, ENCODING_1);
		assertEquals("{\"datasources\":\"[{\"port\":1001,\"host\":\"127.0.0.1\",\"encoding\":\"BINARY\"}]\"}", controller.getDataSources());

		controller.addDataSource(HOST_2, PORT_2, ENCODING_2);
		assertEquals("{\"datasources\":\"[{\"port\":1001,\"host\":\"127.0.0.1\",\"encoding\":\"BINARY\"}, {\"port\":1002,\"host\":\"localhost\",\"encoding\":\"STRING\"}]\"}", controller.getDataSources());

		controller.removeDataSource(HOST_1, PORT_1, ENCODING_1);
		assertEquals("{\"datasources\":\"[{\"port\":1002,\"host\":\"localhost\",\"encoding\":\"STRING\"}]\"}", controller.getDataSources());

		controller.removeDataSource(HOST_2, PORT_2, ENCODING_2);
		assertEquals("{\"datasources\":\"[]\"}", controller.getDataSources());
	}
}
