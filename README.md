# Telemetry challenge project

The challenge project consists of building a telemetry visualisation solution for two different satellites. Each satellite has its own encoding described below. The solution must allow users to explore telemetry data from different time ranges.

The ground station provider offers a separate TCP server for each satellite. Packets are sent over this connection without additional framing. The TCP server will accept any number of connections but only the last connected client for each satellite will receive telemetry.

# How is it built

The solution is built with [Java](https://www.java.com/en/), [Spring boot](https://spring.io/projects/spring-boot), [InfluxDB](https://www.influxdata.com/), [Grafana](https://grafana.com/), [SQLite](https://www.sqlite.org/index.html) and [Docker](https://www.docker.com/).
It is composed of several components that collaborate to solve the challenge.
These components interact with each other, as indicated in the following diagram:

![](./images/architecture.png)

The following is a brief description of the responsibility of each component
- **Data sources:** TCP servers that broadcast data from the satellites
- **Api:** Responsible for receiving requests to managing data sources
- **Telemetry:** Responsible for managing collectors for active data sources
- **Collectors:** Responsible for collecting data issued by a data source
- **Health monitor:** Responsible for reconnecting the collectors that have a problem
- **SQLite:** Storage for active data sources to resume them when the application starts
- **Register:** Responsible for the persistence of data and events
- **Log:** Storage for events
- **InfluxDB:** Storage for data points
- **Grafana:** Data visualization tool

# How to run it

Running it for the first time is a little tricky, after that it is just a command.
1. In order to compile the application we need an InfluxDB, we start it with the following command
`docker run -p 8086:8086 influxdb:1.7.10-alpine`
2. To compile the application, in another terminal we go to the project folder and execute the following command `./mvnw package && java -jar target/telemetry-0.1.0.jar`
3. After a successful build, we must shut down InfluxDB from step 1, to free the socket
4. We create the Docker of the application with the following command `docker build -t ipasquini/telemetry .`
5. In the build/ subdirectory, we create the Docker of the TCP server for the satellites with the following command `docker build -t build/tcp .`
6. Back in the project directory, every time we want to start the solution, we can do it with the following command `docker-compose up`
7. We enter to http://localhost:3000/, with admin as username and password
8. [On the left panel wheel](https://grafana.com/docs/grafana/latest/datasources/influxdb/), we go to _Data Sources_ -> _Add data source_ -> _InfluxDB_
9. Configure it as follows:
- **Url:** http://localhost:8086
- **Access:** Browser
- **Database:** telemetry
- **User:** root
- **Password:** root
- Click on _Save and test_
10. [On the left panel plus sign](https://grafana.com/docs/grafana/latest/dashboards/export-import/), go to _Import_ -> _Upload JSON file_, select the file model.json in the project directory and _Import_

# How to use it

We have an API to manage the data sources.
When we are using the solution in Docker, the host must be _tcp_ as it is the container name of the TCP server of the satellites.
- To add data sources, we can do it as follows:
```
curl -X POST 'localhost:8080/datasource' -d "host=tcp&port=8000&encoding=STRING"
{"added":"{"port":8000,"host":"tcp","encoding":"STRING"}"

curl -X POST 'localhost:8080/datasource' -d "host=tcp&port=8001&encoding=BINARY"
{"added":"{"port":8001,"host":"tcp","encoding":"BINARY"}"}
```
- To list the active data sources, we can do it as follows:
```
curl -X GET 'localhost:8080/datasource'
{"datasources":[{"port":8000,"host":"tcp","encoding":"STRING"},{"port":8001,"host":"tcp","encoding":"BINARY"}]}
```
- To remove a data source, we can do it as follows:
```
curl -X DELETE 'localhost:8080/datasource' -d "host=tcp&port=8001&encoding=BINARY"
{"removed":"{"port":8001,"host":"tcp","encoding":"BINARY"}"}
```

# How it looks

If we add more data sources of new satellites, new [panels](https://grafana.com/docs/grafana/latest/panels/) will automatically appear in the [dashboard](https://grafana.com/docs/grafana/latest/dashboards/), it will not be necessary to add them manually. The same happens if new errors appear. The final result will be similar to the following image:

![](./images/grafana.png)
Every time you start the solution, you can directly enter this dashboard and see how the active data sources begin to appear in the dashboards.